from setuptools import setup, find_packages

setup(name='catalogc',
      version='0.4.0',
      description='Catalog Client',
      long_description='aa',
      url='https://git.terradue.com/air/m.git',
      author='Fabrice Brito',
      author_email='fabrice.brito@terradue.com',
      license='EUPL License',
      packages=find_packages(),
      include_package_data=True,	
      zip_safe=False)
