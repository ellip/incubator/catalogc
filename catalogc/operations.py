from jinja2 import Template
import numpy as np


class OperationFactory:
    """
    OperationFactory
    """
    
    def __init__(self):
        self._operations = {}

    def register_operation(self, operation):
        self._operations[operation.__name__] = operation

    def get_operation(self, operation):
        operation = self._operations.get(operation)
        if not operation:
            raise ValueError(operation)
        return operation()
            
def get_intersection(row, aoi):
    """
    get_intersection
    """
    return (row['geometry'].intersection(aoi).area / aoi.area) * 100


class AoiAnalysisOperation:
    """
    AoiAnalysisOperation
    """
    def __init__(self):
        pass
    
    def pre_correlate(self, collection, operation, filters):

        return filters

    def correlate(self, collection, operation, dataset):

        dataset['aoi_intersec'] = dataset.apply(lambda row: get_intersection(row, 
                                                                             operation['aoi']), 
                                              axis=1)

        return dataset
    
class ReferenceOperation:
    """
    ReferenceOperation
    """
    def __init__(self):
        pass
    
    def pre_correlate(self, collection, operation, filters):

        ref_prd = collection.feed(operation['self'])

        context = dict()

        for key, value in ref_prd.iloc[0].to_dict().items():

            if key == 'geom':
                context['{}'.format(key)] = value.wkt
            else:
                context['{}'.format(key)] = value

        # Update the search parameters with the context    
        for key, value in filters.items():

            try:
                filters[key] = str(Template(value).render(context))  
            except TypeError:
                pass

        if 'time_delta' in operation.keys():

            step = 'D'
            if len(operation['time_delta']) == 3:
                step = operation['time_delta'][2]

            filters['start'] = (ref_prd.iloc[0].startdate_dt + 
                                np.timedelta64(operation['time_delta'][0], 'D')).strftime('%Y-%m-%dT%H:%M:%SZ')

            filters['stop']  = (ref_prd.iloc[0].startdate_dt + 
                                np.timedelta64(operation['time_delta'][1], 'D')).strftime('%Y-%m-%dT%H:%M:%SZ')

        return filters
    
    def correlate(self, collection, operation, dataset):
        """
        correlate
        """
    
        if 'intersection_threshold' in operation.keys():
            assert 'wkt' in collection._fields.split(','), 'To use intersection with reference product, include \'wkt\' in the fields'

            ref_prd = collection.feed(operation['self'])

            dataset['reference_intersec'] = dataset.apply(lambda row: get_intersection(row, 
                                                                                     ref_prd.geometry.values[0]), 
                                                          axis=1)

            dataset = dataset[dataset['reference_intersec'] >= float(operation['intersection_threshold'])].reset_index(drop=True)

        return dataset
    
 

