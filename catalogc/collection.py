import geopandas as gp
from shapely.wkt import loads

import pandas as pd
import subprocess
import re
from packaging import version
from lxml import etree
import requests
from .operations import *
from shapely.geometry import Point

class Collection:
    
    def __init__(self, osd, username=None, api_key=None, **kwargs):
        """ 
        Discovery initialization 
        """

        if username is None and api_key is None:
            
            self._creds = None
        
        else:    
            
            self._creds = ':'.join([username, api_key])

        self._username = username
        self._api_key = api_key
        
        self.osd = osd
        
        if self.osd is not None:
            self.description = self._params()
        else:
            self.description = None
            
        # check if the options are set
        if 'fields' in kwargs.keys():
            self._fields = kwargs['fields']
        else:
            self._fields = ('identifier,self,wkt,startdate,enddate,' 
                            'enclosure,track,productType,swathIdentifier,platform')
        
        if 'timeout' in kwargs.keys():
            self._timeout = kwargs['timeout']
        else:
            self._timeout = 20000
                
        if 'all_enclosures' in kwargs.keys():
            self._all_enclosures = kwargs['all_enclosures'] 
        else:
            self._all_enclosures = False      
        
        if 'model' in kwargs.keys():
            self._model = kwargs['model'] 
        else:
            self._model = 'EOP'    
                
        self._factory = OperationFactory()
        self._factory.register_operation(ReferenceOperation)
        self._factory.register_operation(AoiAnalysisOperation)
   
    def register_operation(self, operation):
        
        self._factory.register_operation(operation)
        
    
    # this is a private method
    def search(self, end_point, params, output_fields, model='GeoTime',
               out_format=None, pagination=20, timeout=20000, creds=None, 
               alternative=None, max_retries=None, all_enclosures=False,
               output=None):
        """ 
        This function uses the opensearch client in order to retrieve results from an opensearch endpoint 
        """
            
#        Args:
#        end_point -- (str or list of str, coma separated):
#                  the URL(s) exposing the opensearch interface
##        params -- endpoint opensearch query params
 #       output_fields -- model output metadata keywords
#        model -- data model of the results for the query. Options are: Geotime (default), EOP, Scihub
#              pagination (integer):
#                  specifies the pagination number for search loops. Default: 20
#              timeout:
#                   specifies query timeout (milliseconds, default timeout=20000) 
#              creds:
#                  a string representing the credentials with format username:password (default creds=':')
#              alternative (default alternative=None):
##                  Altenative query: Instead of making a parallel multi search in
 #                 case of multiple URL, it tries the URL until 1 returns results
##              max_retries (optional, defautlt=5):
#                  specifies the number of retries if the action fails 
##              all_enclosures (optional. default=False):
#                  if set to True returns all available enclosures
#              output:
#                  if set, redirect the stdout to the 'output' file  
#            Returns:
#               (list): the list of the retrieved data (metadata keywords or atom feed).
#            
#            Raises:
#              AssertionError: when alternative=True and end_point is only one URL.
#        """

        cmd = '/usr/bin/opensearch-client'

        options = [cmd]
        options.extend(['-h'])
        # executing the call
        p = subprocess.Popen(options,
            stdout=subprocess.PIPE,
            stdin=subprocess.PIPE,
            stderr=subprocess.PIPE)

        res, err = p.communicate()
        
        versionline = err.decode().rstrip('\n').split('\n')[0]
        pattern = re.compile("OpenSearchClient\.exe \(v(?P<version>[^\)]*)\).*")
        versionOSCli = pattern.search(versionline).group('version')
        
        options = [cmd]
        
        if (version.parse(versionOSCli) >= version.parse("1.8.23")):
            options.extend(['-qo'])

        # params
        if isinstance(params, dict):
            for my_key in params:
                options.extend(['-p', my_key + '=' + str(params[my_key]) ])

        # model
        options.extend(['-m', model ])

        # pagination
        options.extend(['--pagination', str(pagination) ])

        # timeout
        options.extend(['--time-out', str(timeout) ])
        
        if alternative:
            assert (len(end_point.split(',')) > 1 and len(creds.split(',')) >=2), "Not enough endpoints and/or credentials for alternative option"
            options.extend(['--alternative'])
        
        # credentials 
        if creds is not None :
            options.extend(['-a', creds])
        
        if max_retries is not None:
            options.extend(['--max-retries', str(max_retries)])

        if all_enclosures:
            options.extend(['--all-enclosures'])
            
        if out_format is not None:
            options.extend(['-f', out_format])
        
        if output is not None:
            options.extend(['-o', output])
         
        # end point
        options.extend([end_point])
        
        if not output_fields:
            if model == 'GeoTime':
                output_fields = ('self,title,enclosure,identifier,startdate,'
                                 'enddate,published,updated,related,wkt')
            if model == 'EOP':
                output_fields = ('title,enclosure,identifier,startdate,enddate,published,' 
                                 'updated,related,productType,parentIdentifier,orbitNumber,orbitDirection,'
                                'track,frame,swathIdentifier,platform,operationalMode,'
                                 'polarisationChannels,wrsLongitudeGrid,processingLevel,wkt')
            if model == 'Scihub':
                output_fields = ('wkt,title,enclosure,identifier,startdate,enddate,' 
                                 'published,updated,related,self,link,productType,parentIdentifier,'
                                 'orbitNumber,orbitDirection,track,frame,swathIdentifier,platform,' 
                                 'operationalMode,polarisationChannels,wrsLongitudeGrid,' 
                                 'wrsLatitudeGrid,processingLevel,id')
        
        # removing spaces before and after each field 
        output_fields = ','.join([field.strip() for field in output_fields.split(',') if field.strip() != ''])
        
        # patching the output field string in order
        # to put the wkt (if exists) as last field
        # this is needed until the opensearch-client
        # will be able to choose a field separator
        if output_fields.find('wkt') > 0:
            output_fields = (output_fields.replace('wkt', '') + ',wkt').replace(',,', ',')

        options.extend([output_fields])
        
        # executing the call
        proc = subprocess.Popen(options,
                                stdout=subprocess.PIPE,
                                stdin=subprocess.PIPE,
                                stderr=subprocess.PIPE,
                                universal_newlines=True)

        res, err = proc.communicate()

        if proc.returncode:
            raise Exception('subprocess returned: ' + str(proc.returncode) + '\n' + err)

        if output_fields != '{}':
            # preapring the output
            my_res = []
            my_output = res.rstrip('\n').split('\n')

            pattern = re.compile(r'''((?:[^,"']|"[^"]*"|'[^']*')+|,)''', re.S)
            
            for my_item in my_output:
                my_test = {}
                my_data = pattern.findall(my_item)
                data = []
                prev = ','
                for elem in my_data:
                    if elem == ',':
                        if prev == ',':
                            data.append('')
                    else:
                        data.append(elem)
                    prev = elem

                if data:
                    for i, my_field in enumerate(output_fields.split(',')):
                        try:
                            my_test[my_field.strip()] = data[i].strip('"') if data[i] else ''
                        except:
                            my_test[my_field.strip()] = ''
                    # adding the dict to the array
                    my_res.extend([my_test])
            res = my_res

        return res

    def _intersection(self, row, aoi):

        return (row['geometry'].intersection(aoi).area / aoi.area) * 100


    def feed(self, *input_references):
        """ This function returns a geopandas dataframe with the input references metadata. """

#        Args:
#            input_references: one or more references to entries in the catalogue or 
#                              collections filtered
#            kwargs: additional parameters for settings query options:
#                    fields: the metadata fields to return in the geopandas dataframe. 
#                    Defaults to 'identifier,self,wkt,startdate,enddate,enclosure,cc,productType,track'
#                    timeout: opensearch-client timeout in milliseconds
#                    all_enclosures: request all enclosures 
#                    model: set the metadata model, EOP or GeoTime. Defaults to GeoTime
#
#        Returns
#            geopandas dataframe.
#
#        Raises:
#            None.
#        """
            
        temp_results = []
        temp_gpd = None
        
        for index, ref in enumerate(input_references):

            if isinstance(ref, gp.GeoDataFrame):
                
                if temp_gpd is None:
                    temp_gpd = ref
                else:
                    temp_gpd = gp.GeoDataFrame(pd.concat([temp_gpd, ref], ignore_index=True))
            else:
                filters = dict()

                filters['do'] = 'terradue'

                for entry in self.search(end_point=ref.replace(',', '%2C'), 
                                         params=filters,
                                         output_fields=self._fields, 
                                         creds=self._creds,
                                         timeout=self._timeout,
                                         all_enclosures=self._all_enclosures,
                                         model=self._model):
            
                    temp_results.append(entry)  

        temp_products = gp.GeoDataFrame(temp_results)

        if temp_gpd is None:
            
            dataset = gp.GeoDataFrame(temp_results)
            
        else:
        
            drop_columns = list(set(list(temp_gpd.columns)) - set(temp_products.columns)) 

            dataset = pd.concat([temp_gpd, temp_products], 
                                ignore_index=True, 
                                sort=False).drop(columns=drop_columns)
       
        if 'startdate' in dataset.columns:
            dataset['startdate_dt'] = pd.to_datetime(dataset.startdate)

        if 'enddate' in dataset.columns:
            dataset['enddate_dt'] = pd.to_datetime(dataset.enddate)

        if 'wkt' in dataset.columns:
            dataset = dataset.rename(index=str, columns={'wkt': 'geometry'})
            dataset['geometry'] = dataset['geometry'].apply(lambda x: loads(x) 
                                                              if pd.notnull(x) 
                                                              else Point())
    
        return dataset   
    
    def filter(self, **kwargs):
        """This function does a search using the search_params

        Args:
            options: a dictionary to set the options. The key/value pairs may include:
                    fields: the metadata fields to return in the geopandas dataframe. 
                    Defaults to 'identifier,self,wkt,startdate,enddate,enclosure,cc,productType,track'
                    timeout: opensearch-client timeout in milliseconds
                    all_enclosures: request all enclosures 
                    model: set the metadata model, EOP or GeoTime. Defaults to GeoTimes
            kwargs: define the query parameters and values

        Returns
            geopandas dataframe.

        Raises:
            None.
        """
        filters = kwargs
        
        dataset = gp.GeoDataFrame(self.search(end_point=self.osd,
                                             params=filters,
                                             output_fields=self._fields, 
                                             creds=self._creds,
                                             timeout=self._timeout,
                                             all_enclosures=self._all_enclosures,
                                             model=self._model)) 

        if dataset.empty:
            print('No results!')
            return dataset

        # update the geodataframe column type
        if 'startdate' in self._fields.split(','):
            dataset.startdate = pd.to_datetime(dataset.startdate) #.astype(dt.datetime)

        if 'enddate' in self._fields.split(','):
            dataset.enddate = pd.to_datetime(dataset.enddate) #.astype(dt.datetime)

        if 'wkt' in self._fields.split(','):
            dataset = dataset.rename(index=str, columns={'wkt': 'geometry'})
            dataset['geometry'] = dataset['geometry'].apply(loads)


        return dataset

    
    def correlate(self, operation, **kwargs):
        """This function does a search using the search_params

        Args:
            operation: a dictionary defining the correlation function key/value pairs
            kwargs: define the query parameters and values

        Returns
            geopandas dataframe.

        Raises:
            None.
        """
    
        filters = kwargs

        # correlation operation
        op = self._factory.get_operation(operation['name'])

        filters = op.pre_correlate(self, operation, filters)

        dataset = gp.GeoDataFrame(self.search(end_point=self.osd,
                                              params=filters,
                                              output_fields=self._fields, 
                                              creds=self._creds,
                                              timeout=self._timeout,
                                              all_enclosures=self._all_enclosures,
                                              model=self._model))

        if dataset.empty:
            print('No results')
            return search

        # update the geodataframe column type
        if 'startdate' in self._fields.split(','):
            dataset.startdate = pd.to_datetime(dataset.startdate) #.astype(dt.datetime)

        if 'enddate' in self._fields.split(','):
            dataset.enddate = pd.to_datetime(dataset.enddate) #.astype(dt.datetime)

        if 'wkt' in self._fields.split(','):
            dataset = dataset.rename(index=str, columns={'wkt': 'geometry'})
            dataset['geometry'] = dataset['geometry'].apply(loads)

        dataset = op.correlate(self, operation, dataset)
       
        return dataset

    
    def _params(self):

        oss_ns = {'a':'http://www.w3.org/2001/XMLSchema',
                  'b':'http://www.w3.org/2001/XMLSchema-instance',
                  'c':'http://a9.com/-/opensearch/extensions/time/1.0/',
                  'd':'http://www.opengis.net/eop/2.0',
                  'e':'http://purl.org/dc/terms/',
                  'f':'http://a9.com/-/spec/opensearch/extensions/parameters/1.0/',
                  'g':'http://purl.org/dc/elements/1.1/',
                  'h':'http://www.terradue.com/opensearch',
                  'i':'http://a9.com/-/opensearch/extensions/geo/1.0/',
                  'j':'http://a9.com/-/spec/opensearch/1.1/'}
       
        if self._username is None:
            
            oss_content = etree.fromstring(requests.get(self.osd).content)
            
        else:
            
            oss_content = etree.fromstring(requests.get(self.osd,  
                                                        auth=(self._username,
                                                              self._api_key)).content)

        url_template_element = oss_content.xpath('/j:OpenSearchDescription/' 
                                                 + 'j:Url[@type="application/atom+xml"]',
                                                 namespaces=oss_ns)[0]

        parameters = dict()

        for index, parameter in enumerate(url_template_element.xpath('.//f:Parameter', 
                                                                     namespaces=oss_ns)):

            parameters[parameter.attrib['name']] = {'title' : parameter.attrib['title'],
                                                    'value' : parameter.attrib['value']}

            options = []
            for option in parameter.xpath('.//f:Option', namespaces=oss_ns):

                options.append(option.attrib['value'])

            parameters[parameter.attrib['name']] = {'title' : parameter.attrib['title'],
                                                    'value' : parameter.attrib['value'],
                                                    'options' : options}
        
        return parameters
    
  
    