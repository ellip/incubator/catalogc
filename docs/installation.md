## Installation

Install the *catalogc* Python module with conda.

```bash
conda install -y catalogc 
```

Or 

```bash
conda install -n <your environment> -y catalogc 
```

### Dependencies