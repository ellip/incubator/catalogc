## Catalogc - an Opensearch client 

Try it on binder: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/ellip%2Fincubator%2Fcatalogc/master?urlpath=lab) 

You'll need:

- A Terradue username 

- A Terradue API key

### Build

#### With setuptools

```bash
python setup.py install
```

**Note** your environment must have all the dependencies installed

#### With conda

```bash
conda build .
```

Then install the build package with:

```bash
conda create -n catc -c conda-forge -c terradue -c file://<local build channel path>/conda-bld catalogc
```